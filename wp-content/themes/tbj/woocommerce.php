<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="m8">
			<?php
				woocommerce_content();
			?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>