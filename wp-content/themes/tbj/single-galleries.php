<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="gallery row" role="main">
		<div class="m8 main-content"><div class="inner">
			<?php echo '<h2 class="page-title">'.get_the_title().'</h2>'; ?>
			<?php
				while (have_posts()) {
					the_post();
					get_template_part('content', 'single-gallery');
				}
			?>
		</div></div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>
