<?php
/**
 * Template Name: Home Page Template
 * Template Post Type: page
 * Description: Custom home page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="row" role="main">
		<div class="m8">
			<?php
				while(have_posts()) {
					the_post();

					echo '<h2 class="visually-hidden">'.get_the_title().'</h2>';
					echo '<div class="content-container">';
						the_content();
					echo '</div>';
				}

				echo '<div class="feats">';
					dynamic_sidebar( 'sidebar-2' );
				echo '</div>';
			?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>