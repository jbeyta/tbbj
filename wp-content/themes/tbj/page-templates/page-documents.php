<?php
/**
 * Template Name: Documents w/ categories Page Template
 * Template Post Type: page
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div role="main" class="docs-cats row">
		<div class="m8">
			<?php
				while(have_posts()) {
					the_post();
					the_content();
					echo '<hr>';
				}
			?>
			<?php
				$post_type = 'documents';
				$taxonomy = 'documents_categories';

				// get category terms
				$terms = get_terms( $taxonomy );
				if(!empty($terms)) {
					foreach( $terms as $term ) {
						// display the terms
						echo '<h3 class="section-header">'.$term->name.'</h3>';
						echo '<div>';
						// get the posts
							$args = array(
								'post_type' => $post_type,
								'posts_per_page' => -1,
								'orderby' => 'title',
								'order' => 'ASC',
								'tax_query' => array(
									array(
										'taxonomy' => $taxonomy,
										'field' => 'slug',
										'terms' => $term
									)
								)
							);
							$posts = new WP_Query( $args );
							if( $posts->have_posts() ) {
								while( $posts->have_posts() ) {
									$posts->the_post();

									get_template_part('content', $post_type);

								} // end while have_posts
							}
							wp_reset_query();
						echo '</div>';
					} // end foreach $terms

					// get docs in no category

					$args = array(
						'post_type' => $post_type,
						'posts_per_page' => -1,
						'orderby' => 'title',
						'order' => 'ASC',
						'tax_query' => array(
							array(
								'taxonomy' => $taxonomy,
								'terms'    => get_terms( $taxonomy, array( 'fields' => 'ids'  ) ),
								'operator' => 'NOT IN'
							)
						)
					);
					$posts = new WP_Query( $args );
					if( $posts->have_posts() ) {
						while( $posts->have_posts() ) {
							$posts->the_post();

							get_template_part('content', $post_type);

						} // end while have_posts
					}
					wp_reset_query();
				} else {
					$args = array(
						'post_type' => $post_type,
						'posts_per_page' => -1,
						'orderby' => 'title',
						'order' => 'ASC'
					);
					$posts = new WP_Query( $args );
					if( $posts->have_posts() ) {
						while( $posts->have_posts() ) {
							$posts->the_post();

							get_template_part('content', $post_type);

						} // end while have_posts
					} else { // end if have_posts
						echo '<p>No '.$post_type.' right now. Check back soon!</p>';
					}
					wp_reset_query();
				}
			?>
		</div>

		<?php get_sidebar(); ?>

	</div>

<?php get_footer(); ?>