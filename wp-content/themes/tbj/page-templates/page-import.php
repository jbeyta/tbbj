<?php
/**
 * Template Name: import Template
 * Template Post Type: page
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div role="main" class="docs-cats row">
		<div class="s12">
			<?php
				$xml = simplexml_load_file(get_template_directory_uri().'/products.xml');
				
				foreach($xml as $key => $prod) {
					// echo_pre($prod);

					// if($key > 2) {
						// return;
					// }

					$post_data = array(
						'post_title' => (string) $prod->pName,
						'post_content' => (string) $prod->pLongDescription,
						'post_status' => 'publish',
						'post_type' => 'product',
					);

					// echo_pre($post_data);

					// $postid = wp_insert_post($post_data, true);
					echo_pre($postid);

					if(!empty($postid)) {
						$Price = (string) $prod->pPrice;
						$Weight = (string) $prod->pWeight;
						$sku = (string) $prod->pID;

						update_post_meta($postid, '_sku', $sku);
						update_post_meta($postid, '_regular_price', $Price);
						update_post_meta($postid, '_weight', $Weight);

						$image_remote_url = (string) $prod->pLargeImage;

						// Add Featured Image to Post
						$image_url        = 'http://www.texasbestbeefjerky.com/'.$image_remote_url; // Define the image URL here
						$image_name       = str_replace('prodimages/', '', $image_remote_url);
						$upload_dir       = wp_upload_dir(); // Set upload folder
						$image_data       = file_get_contents($image_url); // Get image data
						$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
						$filename         = basename( $unique_file_name ); // Create image file name

						// Check folder permission and define file location
						if( wp_mkdir_p( $upload_dir['path'] ) ) {
						    $file = $upload_dir['path'] . '/' . $filename;
						} else {
						    $file = $upload_dir['basedir'] . '/' . $filename;
						}

						// Create the image  file on the server
						file_put_contents( $file, $image_data );

						// Check image file type
						$wp_filetype = wp_check_filetype( $filename, null );

						// Set attachment data
						$attachment = array(
							'post_mime_type' => $wp_filetype['type'],
							'post_title'     => sanitize_file_name( $filename ),
							'post_content'   => '',
							'post_status'    => 'inherit'
						);

						// Create the attachment
						$attach_id = wp_insert_attachment( $attachment, $file, $postid );

						// Include image.php
						require_once(ABSPATH . 'wp-admin/includes/image.php');

						// Define attachment metadata
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

						// Assign metadata to attachment
						wp_update_attachment_metadata( $attach_id, $attach_data );

						// And finally assign featured image to post
						set_post_thumbnail( $postid, $attach_id );
					}
				}
			?>
		</div>
	</div>

<?php get_footer(); ?>