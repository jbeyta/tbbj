<?php
// page header
// global $wp_query;

$title = get_the_title();
if(is_404()) {
	$title = 'Not Found: 404';
}

if(is_home() || get_post_type() == 'post') {
	$pageid = get_option('page_for_posts');
	$title = get_the_title($pageid);

	if(empty($pageid)) {
		$title = 'News';
	}
}

if(is_woocommerce()) {
	$title = 'Shop';
}

if(is_search()) {
	$title = 'Search';
}

?>

<div class="page-header">
	<div class="row">
		<div class="s12">
			<h2 class="page-title"><?php echo $title; ?></h2>
		</div>
	</div>
	<?php
		$img = get_the_post_thumbnail_url();
		// $cropped = aq_resize($img, 1400, 600, true, true, true);
		if(empty($img) || is_singular('product') || get_post_type() == 'product') {
			$img = cw_options_get_option('_cwo_page_header');
		}
		if(!empty($img)) {
			echo '<div class="page-bg" style="background: center center no-repeat url('.$img.'); background-size: cover;"></div>';
		}
	?>
</div>