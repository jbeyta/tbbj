<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="m8">
			<?php
				while ( have_posts() ) {
					the_post();
					get_template_part( 'content', get_post_type() );
					// comments_template( '', true );
				}
			?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>