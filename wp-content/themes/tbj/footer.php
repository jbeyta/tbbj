<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
			<footer role="contentinfo">
				<div class="flex row">
					<div class="m6 copy">
						<p>&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>, All Rights Reserved.</p>
						<?php echo do_shortcode('[contact_info address="hide" phone2="show"]'); ?>
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'footer',
									'container' => '',
									'menu_class' => 'styleless footer-nav',
									'depth' => 1,
									'fallback_cb' => 'wp_page_menu',
								)
							);
						?>
					</div>

					<div class="m4" style="text-align: center;">
						<img src="<?php echo get_template_directory_uri(); ?>/img/usda.gif" alt="" />
						<p style="color: #fff;">Texas Best Beef Jerky is prepared in our own USDA-Inspected facility!</p>
					</div>

					<div class="m2 cw">
						<a class="cw-logo" href="http://crane-west.com/"><?php get_template_part('img/siteby', 'cranewest.svg'); ?></a>
					</div>
				</div>
			</footer>
		</div> <!-- end off-canvas-wrap-inner -->	
	</div> <!-- end off-canvas-wrap -->
	<!-- WP_FOOTER() -->
	<?php wp_footer(); ?>
</body>
</html>