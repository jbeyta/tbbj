<?php
// documents contents
$file = get_post_meta($post->ID, '_cwmb_file', true);
$subtitle = get_post_meta($post->ID, '_cwmb_subtitle', true);

if(!empty($file)) {
	echo '<div class="doc-download">';
		echo '<h5><a href="'.$file.'" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp;'.get_the_title().'</a></h5>';

		if(!empty($subtitle)) {
			echo '<p>'.$subtitle.'</p>';
		}
	echo '</div>';
}