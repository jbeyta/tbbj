<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
	<aside class="widget-area m4" role="complementary">
		<?php
			if(
				is_woocommerce()
				|| is_shop()
				|| is_product_category()
				|| is_product_tag()
				|| is_product()
				|| is_cart()
				|| is_checkout()
			) {
				echo '<div class="widget">';
					echo '<h3 class="widget-title">Product Categories</h3>';
					echo do_shortcode('[category_select taxonomy="product_cat" size="12"]');
				echo '</div>';
			}
		?>

		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside>