<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="m8">
			<p>Content not found. Please make sure the url is correct.</p>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>