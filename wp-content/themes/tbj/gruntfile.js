
module.exports = function(grunt) {
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				options: {
					sourceComments: 'map',
				},
				files: {
					'css/style.css': 'css/scss/style.scss',
					'css/scss/_fontawesome.scss': 'node_modules/font-awesome/scss/font-awesome.scss',
					'css/admin/admin-style.css': 'css/scss/admin-style.scss' 
				}
			}
		},

		cssmin: {
			css:{
				src: 'css/style.css',
				dest: 'css/style.min.css'
			}
		},

		jshint: {
			beforeconcat: ['js/*.js']
		},

		concat: {
			dist: {
				src: [
					'js/plugins/*',
					'js/cw-main.js',
					'js/cw-slides.js'
				],
				dest: 'js/production.js'
			}
		},

		uglify: {
			my_target: {
				options: {
					sourceMap: true,
					sourceMapName: 'js/sourcemap.map'
				},
				files: {
					'js/production.min.js': ['js/production.js']
				}
			}
		},

		copy: {
			main: {
				files :[{
					expand: true,
					cwd: 'node_modules/font-awesome/fonts/',
					src: '*',
					dest: 'fonts/'
				}]
			}
		},

		autoprefixer: {
			options: {
				browsers: ['last 8 versions']
			},
			dist: {
				files: {
					'css/style.css': 'css/style.css'
				}
			}
		},

		watch: {
			options: {
				livereload: true,
			},
			scripts: {
				files: ['js/*.js'],
				tasks: ['concat', 'uglify'],
				options: {
					spawn: false,
				}
			},
			css: {
				files: ['css/scss/*.scss'],
				tasks: ['sass', 'autoprefixer', 'cssmin'],
				options: {
					spawn: false,
				}
			},
			php: {
				files: ['*.php'],
				options: {
					spawn: false
				}
			},
		},

		connect: {
			server: {
				options: {
					port: 8000,
					base: './'
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['copy', 'watch']);
};