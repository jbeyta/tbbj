<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?><!DOCTYPE html>

<!--[if lt IE 7]> <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html style="margin-top: 0!important;" class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html style="margin-top: 0!important;" class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head >
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<?php
		$favicon = cw_options_get_option( '_cwo_favicon' );
		if(!empty($favicon)) { echo '<link rel="shortcut icon" type="image/png" href="'.$favicon.'"/>'; }
	?>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">

	<link href="https://fonts.googleapis.com/css?family=Patua+One|Work+Sans:400,700" rel="stylesheet">

	<!-- WP_HEAD() -->
	<?php wp_head(); ?>

	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
	<![endif]-->

	<?php
		$tracking = cw_options_get_option( '_cwo_tracking_code' );
		if(!empty($tracking)) {
			echo $tracking;
		}
	?>
</head>

<body <?php body_class(); ?>>
	<?php
		$ga_code = cw_options_get_option('_cwo_ga');
		if( !empty($ga_code) ) {

		// only put the tracking on code when the site is not on a .dev
		// $extension = pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION);
		?>
			<script type="text/javascript">
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				ga('create', '<?php echo $ga_code; ?>', 'auto');
				ga('send', 'pageview');
			</script>
	<?php } ?>
	<div class="off-canvas-wrap">
		<div class="off-canvas-wrap-inner">
		<?php get_template_part('content', 'browse-happy'); ?>

		<header role="banner">
			<div class="cw-nav-cont">
				<div class="flex-row justify">
					<div class="s3 logo-cont">
						<h1 class="logo">
							<a href="/" title="<?php bloginfo( 'name' ); ?>">
								<?php
									$logo_img = cw_options_get_option( '_cwo_logo' );
									$logo_svg = cw_options_get_option( '_cwo_logo_svg' );

									if(!empty($logo_svg)) {
										echo $logo_svg;
										echo '<span class="visually-hidden">'.get_bloginfo('name').'</span>';
									} elseif(!empty($logo_img)) {
										echo '<img src="'.$logo_img.'" alt="'.get_bloginfo( 'name' ).'" />';
										echo '<span class="visually-hidden">'.get_bloginfo('name').'</span>';
									} else {
										bloginfo( 'name' );
									}
								?>
							</a>
						</h1>
					</div>

					<nav class="s9 cw-nav columns" role="navigation">
						<span class="menu-toggle" data-menu="cw-nav-ul"><i class="fa fa-bars"></i></span>
						<span class="menu-close" data-menu="cw-nav-ul"><i class="fa fa-times"></i></span>
						
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'primary',
									'container' => '',
									'menu_class' => 'menu cf cw-nav-ul',
									'depth' => 2,
									'fallback_cb' => 'wp_page_menu',
									// 'walker' => new Foundation_Walker_Nav_Menu() // not required, use for custom nav stuff
								)
							);
						?>
					</nav>
				</div>
			</div>
		</header>

		<?php
			if(is_front_page()) {
				get_template_part('content', 'slides');
			} else {
				get_template_part('content', 'pageheader');
			}
		?>