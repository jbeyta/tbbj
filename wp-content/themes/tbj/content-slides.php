<?php
	$slides_args = array(
		'post_type' => 'slides',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	$the_slides = new WP_Query($slides_args);

	if($the_slides->have_posts()){
		$mode = cw_slideshow_options_get_option('_cwso_mode');
		$controls = cw_slideshow_options_get_option('_cwso_controls');
		$pager = cw_slideshow_options_get_option('_cwso_pager');
		$speed = cw_slideshow_options_get_option('_cwso_speed');
		$pause = cw_slideshow_options_get_option('_cwso_pause');

		if(empty($mode)) {
			$mode = 'horizontal';
		}

		if(empty($controls)) {
			$controls = 'true';
		}

		if(empty($pager)) {
			$pager = 'false';
		}

		if(empty($speed)) {
			$speed = '500';
		} else {
			$speed = $speed*1000;
		}

		if(empty($pause)) {
			$pause = '5000';
		} else {
			$pause = $pause*1000;
		}

		echo '<div class="cw-slideshow">';
			echo '<ul class="styleless cw-slider" data-mode="'.$mode.'" data-controls="'.$controls.'" data-pager="'.$pager.'" data-speed="'.$speed.'" data-pause="'.$pause.'">';
			while($the_slides->have_posts()) {
				$the_slides->the_post();

				$slide_title = get_post_meta($post->ID, '_cwmb_slide_title', true);
				$slide_image = get_post_meta($post->ID, '_cwmb_slide_image', true);
				$slide_caption = get_post_meta($post->ID, '_cwmb_slide_caption', true);
				$slide_link = get_post_meta($post->ID, '_cwmb_slide_link', true);

				$cropped = aq_resize( $slide_image, 1400, 600, true, true, true );

				if(!empty($cropped)) {
					echo '<li class="slide">';
						if(!empty($slide_link)) { echo '<a href="'.$slide_link.'">'; }

							echo '<img src="'.$cropped.'" alt="" />';

							if(!empty($slide_caption)) {
								echo '<div class="slide-words">';
									echo '<div class="slide-caption"><span class="cap">'.nl2br($slide_caption).'</span><div class="bg"></div></div>'; 
								echo '</div>';
							}

						if(!empty($slide_link)) { echo '</a>'; }
					echo '</li>';
				}
			}
		echo '</ul>';
	echo '</div>'; // cw-slideshow
	}
wp_reset_query(); ?>
