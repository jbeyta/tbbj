<?php
/**
 * CW  functions
 *
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */

// for debugging
function echo_pre($input) {
	echo '<pre>';
	print_r($input);
	echo '</pre>';
}

// // // // // // // // // //// // // // //// // // // //
//
// Uncomment below in the case the CMB2 plugin doesn't work
// Will need to download an update from here: https://github.com/WebDevStudios/CMB2
//
// if ( file_exists(__DIR__.'/lib/cmb2/init.php')) {
// 	require_once __DIR__.'/lib/cmb2/init.php';
// }
//
// // // // //// // // // //// // // // //// // // // //

/* Include walker for wp_nav_menu */
get_template_part('lib/foundation_walker');

// Used for cropping images on the fly.
// Read docs here => https://github.com/syamilmj/Aqua-Resizer/
require_once( 'lib/aq_resizer.php' );

/**
 * Sets up theme defaults
 *
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since CW 1.0
 */
function cw_setup() {
	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'fonts/genericons.css' ) );

	/*
	 * Adds RSS feed links to <head> for posts and comments.
	 */
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Switches default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

	/*
	 * This theme supports all available post formats by default.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
	) );

	/*
	 * Create the main menu location.
	 */
	register_nav_menu( 'primary', __( 'Main Menu', 'cw' ) );
	register_nav_menu( 'footer', __( 'Footer Navigation', 'cw' ) );

	/*
	 * This theme uses a custom image size for featured images, displayed on "standard" posts.
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop

	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'cw_setup' );

/**
 * Import Custom Post Types, etc.
 *
 *
 * @since CW 1.0
 */
get_template_part('lib/cmb2-slider-input/cmb2_field_slider');
get_template_part('lib/cw-options-page');
get_template_part('lib/cw-slideshow-options-page');
get_template_part('lib/cw-contact-page');
get_template_part('lib/cw-cpt-page');
get_template_part('lib/cw-make-post-types');
get_template_part('lib/content', 'states');
get_template_part('lib/metaboxes');
get_template_part('lib/custom-columns');
get_template_part('lib/cw-widgets');
get_template_part('lib/cw-shortcodes');

// uncommment to use cw_mime_type_format() to check mime types
// get_template_part('lib/cw-allowed-file-types');

// uncomment to use fancy_date($date) to format date like: 4 minutes ago, today, yesterday, and then date
// get_template_part('lib/cw-fancy-date');

/**
 * Add REST API support to an already registered post type.
 */
add_action( 'init', 'add_rest_support', 25 );
function add_rest_support() {
	global $wp_post_types;
	
	//be sure to set this to the name of your post type!
	$post_type_name = 'product';
	if( isset( $wp_post_types[ $post_type_name ] ) ) {
		$wp_post_types[$post_type_name]->show_in_rest = true;
		// Optionally customize the rest_base or controller class
		$wp_post_types[$post_type_name]->rest_base = $post_type_name;
		$wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
}

/**
 * Enqueue front end scripts for CW theme.
 */
add_action("wp_enqueue_scripts", "cw_enqueue_frontend", 11);
function cw_enqueue_frontend() {
	// wp_deregister_script('jquery');
	// wp_deregister_script('jquery-migrate');

	$mdrn_mtime = filemtime(dirname(__FILE__) . '/js/vendor/modernizr.js');
	wp_enqueue_script('modernizr', get_template_directory_uri().'/js/vendor/modernizr.js', array('jquery'), $mdrn_mtime, false);

	// Enqueue dist.js with jquery and foudnation as a dependency.
	$js_mtime = filemtime(dirname(__FILE__) . '/js/production.min.js');
	wp_enqueue_script('cw_js', get_template_directory_uri().'/js/production.min.js', array('jquery'), $js_mtime, true);

	$angl_mtime = filemtime(dirname(__FILE__) . '/js/vendor/angular.min.js');
	wp_enqueue_script('angular', get_template_directory_uri().'/js/vendor/angular.min.js', array('jquery'), $angl_mtime, false);

	$angls_mtime = filemtime(dirname(__FILE__) . '/js/vendor/angular-sanitize.min.js');
	wp_enqueue_script('angular_san', get_template_directory_uri().'/js/vendor/angular-sanitize.min.js', array('angular'), $angls_mtime, false);

	// this cannot be concat with other js
	$app_mtime = filemtime(dirname(__FILE__) . '/js/app.js');
	wp_enqueue_script('cw_app_js', get_template_directory_uri().'/js/app.js', array('angular'), $app_mtime, true);

	// Enqueue the threaded comments reply scipt when necessary.
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/*
	*  Auto-version CSS & JS files, allowing for cache busting when these files are changed.
	*  Avoids using query strings which prevent proxy caching
	*  Adjust paths based on your theme setup. These paths work with Bones theme
	*/

	$mtime = filemtime(dirname(__FILE__) . '/css/style.css');
	wp_register_style( 'cw-stylesheet', get_bloginfo('template_url') . '/css/style.min.css', array(), $mtime, 'all');

	// enqueue the stylesheet
	wp_enqueue_style( 'cw-stylesheet' );

	// Variables for app script
	
	// NOTE: if this isn't working check this https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-rest-api-support-for-custom-content-types/
	$postTypes = array();
	$cpts = get_post_types(array('public' => true), 'objects');
	if(!empty($cpts)) {
		foreach($cpts as $cpt) {
			if(!empty($cpt->slug)) {
				$postTypes[$cpt->slug.'_api'] = get_bloginfo('wpurl').'/wp-json/wp/v2/'.$cpt->slug.'-api';
			}
		}
	}
	$postTypes['posts_api'] = get_bloginfo( 'wpurl' ) . '/wp-json/wp/v2/posts';
	$postTypes['pages_api'] = get_bloginfo( 'wpurl' ) . '/wp-json/wp/v2/pages';
	// uncomment below to add tribe events to the search app
	// $postTypes['tribe_events_api'] = get_bloginfo( 'wpurl' ) . '/wp-json/wp/v2/tribe_events';
	$postTypes['product_api'] = get_bloginfo( 'wpurl' ) . '/wp-json/wp/v2/product';

	wp_localize_script( 'cw_app_js', 'cwSearchAppJS', $postTypes );
}

function cw_enqueue_admin() {
	wp_register_style( 'cw_admin_css', get_template_directory_uri() . '/css/admin/admin-style.css', false, '1.0.0' );
	wp_enqueue_style( 'cw_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'cw_enqueue_admin' );

/**
 * Add REST API support to an already registered post type.
 */
add_action( 'init', 'my_custom_post_type_rest_support', 25 );
function my_custom_post_type_rest_support() {
	global $wp_post_types;
	
	//be sure to set this to the name of your post type!
	$post_type_name = 'product';
	if( isset( $wp_post_types[ $post_type_name ] ) ) {

		$wp_post_types[$post_type_name]->show_in_rest = true;
		// Optionally customize the rest_base or controller class
		$wp_post_types[$post_type_name]->rest_base = $post_type_name;
		$wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';
	}
}

/**
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since CW 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function cw_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'cw_wp_title', 10, 2 );

/**
 * Extends the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 * @since CW 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function cw_body_class( $classes ) {
	// if ( ! is_multi_author() )
	// 	$classes[] = 'single-author';

	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
		$classes[] = 'sidebar';

	// if ( ! get_option( 'show_avatars' ) )
	// 	$classes[] = 'no-avatars';

	return $classes;
}
add_filter( 'body_class', 'cw_body_class' );

/**
 * Register Widget Areas
 *
 * Uncomment and edit to create widget areas where needed.
 * These are default examples so make changes before production.
 *
 * @since CW 1
 *
 */
function cw_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Main Widget Area' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Appears in the sidebar section of the site.' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Front Page Widget Area' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears on the front page.' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'cw_widgets_init' );

/**
 * Remove Admin Menu Items
 * http://codex.wordpress.org/Function_Reference/remove_menu_page
 * @since CW 1.0
 */
function cw_remove_admin_menu_items() {
	// remove_menu_page( 'index.php' );                  //Dashboard
	// remove_menu_page( 'edit.php' );                   //Posts
	// remove_menu_page( 'upload.php' );                 //Media
	// remove_menu_page( 'edit.php?post_type=page' );    //Pages
	// remove_menu_page( 'edit-comments.php' );          //Comments
	// remove_menu_page( 'themes.php' );                 //Appearance
	// remove_menu_page( 'plugins.php' );                //Plugins
	// remove_menu_page( 'users.php' );                  //Users
	// remove_menu_page( 'tools.php' );                  //Tools
	// remove_menu_page( 'options-general.php' );        //Settings

	$hide_posts = cw_pt_get_option( '_cwpt_hide_posts' );
	if(!empty($hide_posts) && $hide_posts == 'on') {
		remove_menu_page( 'edit.php' );
	}

	$current_user = wp_get_current_user();

	if(in_array('site_admin', $current_user->roles)) {
		remove_menu_page( 'tools.php' );
	}

	if(in_array('site_admin_plus', $current_user->roles)) {
		remove_menu_page( 'tools.php' );
		remove_menu_page( 'options-general.php' );
		remove_menu_page( 'plugins.php' ); 
		remove_menu_page( 'themes.php?page=editcss' );
		remove_submenu_page( 'themes.php', 'widgets.php' );
		remove_submenu_page( 'themes.php', 'themes.php' );
		remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Fnav-menus.php' );
		remove_submenu_page( 'themes.php', 'themes.php?page=editcss' );
	}
}
add_action('admin_menu', 'cw_remove_admin_menu_items');

function remove_edit_css() {
	$current_user = wp_get_current_user();
	if(in_array('site_admin_plus', $current_user->roles)) { ?>
		<style>
			.menu-icon-appearance .wp-submenu li:last-child {
				display: none;
			}
			.hide-if-no-customize {
				display: none;
			}
		</style>
	<?php }
}
add_action('admin_menu', 'remove_edit_css');

function list_submenu_admin_pages($parent){
	if(!isset($_GET['dev'])) {
		return;
	}

    global $submenu;

    if ( is_array( $submenu ) && isset( $submenu[$parent] ) ) {
        foreach ( (array) $submenu[$parent] as $item) {
            if ( $parent == $item[2] || $parent == $item[2] )
                continue;
            // 0 = name, 1 = capability, 2 = file
            if ( current_user_can($item[1]) ) {
                $menu_file = $item[2];
                if ( false !== ( $pos = strpos( $menu_file, '?' ) ) )
                    $menu_file = substr( $menu_file, 0, $pos );
                if ( file_exists( ABSPATH . "wp-admin/$menu_file" ) ) {
                    $options[] = "<a href='{$item[2]}'$class>{$item[0]}
                                  </a>";
                } else {
                    $options[] = "<a href='admin.php?page={$item[2]}'>
                                  {$item[0]}</a>";
                }
            }
        }
        return $options;
    }
     echo_pre($submenu);
}
// add_action('admin_menu', 'list_submenu_admin_pages');

function cw_excerpt($content, $limit = 50) {
	if(!empty($content)) {
		$no_tags = strip_tags(strip_shortcodes($content));
		$explode = explode(' ', $no_tags);
		$limited = array_slice($explode, 0, $limit);
		$excerpt = implode(' ', $limited);
	} else {
		$excerpt = 'ERROR: Oops! looks like your content is empty. Make sure you include the content! cw_excerpt($content, $limit)';
	}

	return $excerpt;
}

/**
 * Rename "Posts" to "News"
 *
 * @link http://new2wp.com/snippet/change-wordpress-posts-post-type-news/
 */
 $news_posts = cw_pt_get_option( '_cwpt_news_posts' );
 if(!empty($news_posts) && $news_posts == 'on') {
	 add_action( 'admin_menu', 'pilau_change_post_menu_label' );
	 add_action( 'init', 'pilau_change_post_object_label' );
 }
function pilau_change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'News';
	$submenu['edit.php'][5][0] = 'News';
	$submenu['edit.php'][10][0] = 'Add News';
	$submenu['edit.php'][16][0] = 'News Tags';
	echo '';
}
function pilau_change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'News';
	$labels->singular_name = 'News';
	$labels->add_new = 'Add News';
	$labels->add_new_item = 'Add News';
	$labels->edit_item = 'Edit News';
	$labels->new_item = 'News';
	$labels->view_item = 'View News';
	$labels->search_items = 'Search News';
	$labels->not_found = 'No News found';
	$labels->not_found_in_trash = 'No News found in Trash';
}


/**
 * Remove "Personal Options" from user profile
 *
 * @link http://wpsnipp.com/index.php/functions-php/remove-personal-options-from-user-profiles/
 */
function hide_personal_options(){
echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'form#your-profile > h3:first\').hide(); $(\'form#your-profile > table:first\').hide(); $(\'form#your-profile\').show(); });</script>' . "\n";
}
// add_action('admin_head','hide_personal_options');

// get rid of [...]
function new_excerpt_more( $more ) {
	return '&hellip;';
}
add_filter('excerpt_more', 'new_excerpt_more');

// change "enter title here"
add_filter('gettext','custom_enter_title');
function custom_enter_title( $input ) {

    global $post_type;

    if( is_admin() && 'Enter title here' == $input )
    	if('testimonials' == $post_type )
        	return 'Enter Name Here';
        elseif('staff' == $post_type )
        	return 'Enter Name Here';
        elseif('faqs' == $post_type )
        	return 'Question';

    return $input;
}

// block site admin users from creating administrator users
// take from here http://wordpress.stackexchange.com/questions/4479/editor-can-create-any-new-user-except-administrator
class JPB_User_Caps {

	// Add our filters
	function JPB_User_Caps(){
		add_filter( 'editable_roles', array(&$this, 'editable_roles'));
		add_filter( 'map_meta_cap', array(&$this, 'map_meta_cap'),10,4);
	}

	// Remove 'Administrator' from the list of roles if the current user is not an admin
	function editable_roles( $roles ){
		if( isset( $roles['administrator'] ) && !current_user_can('administrator') ){
			unset( $roles['administrator']);
		}
		return $roles;
	}

	// If someone is trying to edit or delete and admin and that user isn't an admin, don't allow it
	function map_meta_cap( $caps, $cap, $user_id, $args ){
		switch( $cap ){
			case 'edit_user':
			case 'remove_user':
			case 'promote_user':
				if( isset($args[0]) && $args[0] == $user_id )
					break;
				elseif( !isset($args[0]) )
					$caps[] = 'do_not_allow';
				$other = new WP_User( absint($args[0]) );
				if( $other->has_cap( 'administrator' ) ){
					if(!current_user_can('administrator')){
						$caps[] = 'do_not_allow';
					}
				}
				break;
			case 'delete_user':
			case 'delete_users':
			    if( !isset($args[0]) )
			        break;
				$other = new WP_User( absint($args[0]) );
			    if( $other->has_cap( 'administrator' ) ){
					if(!current_user_can('administrator')){
						$caps[] = 'do_not_allow';
					}
				}
				break;
			default:
				break;
		}
		return $caps;
	}

}

$jpb_user_caps = new JPB_User_Caps();

// fancy costumizable pagination
// from http://sgwordpress.com/teaches/how-to-add-wordpress-pagination-without-a-plugin/
//
// usage
//
// if (function_exists('pagination')) {
// 	pagination($posts->max_num_pages);
// }

// IF PAGINTION IS NOT WORKING READ THIS
// make sure you use the right query var, some of the ones on the internet are wrong, even from the wp website. It's 'paged' and not 'page', see below
// $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

function pagination($pages = '', $range = 4) {
	$showitems = ($range * 2)+1;

	global $paged;
	if(empty($paged)) $paged = 1;

	if($pages == '') {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages) {
			$pages = 1;
		}
	}

	if(1 != $pages) {
		echo "<div class=\"pagination\">";
		// echo "<span>Page ".$paged." of ".$pages."</span>";
		if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
		if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

		for ($i=1; $i <= $pages; $i++) {
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
				echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
			}
		}

		if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
		if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
		echo "</div>\n";
	}
}

// uncomment to allow search enging visibility to be turned off
function cw_default_search_engine_visiblity() {
	$extension = pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION);
	if($extension == 'dev' || strpos($_SERVER['SERVER_NAME'], 'crane-west.net')) {
		update_option('blog_public', 0);
	} else {
		update_option('blog_public', 1);
	}
}
add_action('init', 'cw_default_search_engine_visiblity');

// tinymce stuff
function my_mce3_options( $settings ) {
	// echo_pre($settings['formats']);
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Heading 3',
			'block' => 'h3',
		),
		array(  
			'title' => 'Heading 4',
			'block' => 'h4',
		),
		array(  
			'title' => 'Heading 5',
			'block' => 'h5',
		),
		array(  
			'title' => 'Heading 6',
			'block' => 'h6',
		),
		array(  
			'title' => 'Paragraph',
			'block' => 'p',
		),
		// array(  
		// 	'title' => 'Orange',
		// 	'block' => 'span',
		// 	'styles' => array(
		// 		'color' => '#E36F18'
		// 	)
		// ),
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$settings['style_formats'] = json_encode( $style_formats );  

	return $settings;
}
add_filter('tiny_mce_before_init', 'my_mce3_options');

function myplugin_tinymce_buttons($buttons) {
	foreach($buttons as $key => $button) {
		// if($button == 'forecolor'){
		// 	unset($buttons[$key]);
		// }
		if($button == 'formatselect') {
			unset($buttons[$key]);
		}
	}
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter('mce_buttons_2','myplugin_tinymce_buttons');

add_filter( 'embed_oembed_html', 'cw_responsify_embed', 99, 4 );
add_filter( 'video_embed_html', 'cw_responsify_embed' ); // Jetpack
function cw_responsify_embed( $html ) {
	return '<div class="video-container">'.$html.'</div>';
}

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

// echo out promo from a position, menu_order by default
function cw_get_promo($position, $orderby = 'menu_order', $order = 'ASC') {
	if(empty($position)) {
		return;
	}

	global $post;

	$pargs = array(
		'post_type' => 'promos',
		'posts_per_page' => 1,
		'orderby' => $orderby,
		'order' => $order,
		'tax_query' => array(
			array (
				'taxonomy' => 'promos_categories',
				'field' => 'slug',
				'terms' => $position
			)
		)
	);

	$promos = new WP_Query($pargs);
	if($promos->have_posts()){
		while($promos->have_posts()) {
			$promos->the_post();

			$image = get_post_meta($post->ID, '_cwmb_promo_image', true);
			$url = get_post_meta($post->ID, '_cwmb_promo_link', true);

			if(!empty($image))	{
				$cropped = aq_resize( $image, 640, 640, false, true, true );
				if(empty($cropped)) {
					$cropped = $image;
				}
				echo '<div class="promo">';
					if(!empty($url)) { echo '<a href="'.$url.'">'; }
						echo '<img src="'.$cropped.'" alt="" />';
					if(!empty($url)) { echo '</a>'; }
				echo '</div>';
			}
		}
	}

	wp_reset_query();
}

function cw_generate_title( $post_id ){
	global $current_screen;

	$posttype = $current_screen->post_type;

	if($posttype == 'staff'){
		$cw_post_title = $_POST['_first_name'].' '.$_POST['_last_name'];
		
		$post_args = array(
			'ID' => $post_id,
			'post_title' => $cw_post_title
		);

		if(!wp_is_post_revision($post_id)){
			// unhook this function so it doesn't loop infinitely
			remove_action('save_post', 'cw_generate_title');
			// update the post, which calls save_post again
			wp_update_post( $post_args );
			// re-hook this function
			add_action('save_post', 'cw_generate_title');
		}		
	}
}
// add_action('save_post', 'cw_generate_title');

function cw_google_analytics_alert() {
	$ga_code = cw_options_get_option('_cwo_ga');
	if(empty($ga_code)) {
		echo '<div class="notice notice-error is-dismissible" style="padding: 15px;">';
			echo '<h4 style="display: inline-block; margin: 0 0 0 0;">No Google Analytics Code</h4>&nbsp;|&nbsp;<a style="display: inline-block;" href="/wp-admin/admin.php?page=cw_options_options">Click here to add the code</a>';
		echo '</div>';
	}
}
add_action( 'admin_notices', 'cw_google_analytics_alert' );

// default gform notifications
function cw_list_default_gform_notifs() {
	if(!is_admin() || !class_exists( 'GFCommon' )) {
		return;
	}

	$change_these = array();

	$forms = RGFormsModel::get_forms( null, 'title' );
	if(!empty($forms)) {
		foreach( $forms as $form ) {

			if($form->is_active) {

				$form_obj = GFAPI::get_form( $form->id );
				$notifications = $form_obj['notifications'];
				
				if(!empty($notifications)) {
					foreach ($notifications as $key => $data) {

						if($data['to'] == '{admin_email}') {
							echo '<div class="notice notice-error is-dismissible" style="padding: 15px;">';
								echo '<h3 style="margin: 0 0 10px 0;">Gravity Forms Notification</h3><h4 style="margin: 0 0 10px 0;">Default "Send to Email" Detected</h4> Form title: <b>"'.$form->title.'"</b>, Notification title: <b>"'.$data['name'].'"</b> &nbsp;|&nbsp;<a style="display: inline-block;" href="/wp-admin/admin.php?page=gf_edit_forms&view=settings&subview=notification&id='.$form->id.'&nid='.$key.'">Click here to edit this notification</a>';
							echo '</div>';
						}
					}
				}

			}
		}
	}
}
add_action( 'admin_notices', 'cw_list_default_gform_notifs' );


function addPriceSuffix($format, $currency_pos) {
	switch ( $currency_pos ) {
		case 'left' :
			$currency = get_woocommerce_currency();
			$format = '%1$s%2$s&nbsp;' . $currency;
		break;
	}
 
	return $format;
}
 
add_action('woocommerce_price_format', 'addPriceSuffix', 1, 2);

// https://www.sitepoint.com/community/t/woocommerce-add-sku-to-checkout-order-received-page/253108/3
add_action( 'woocommerce_add_order_item_meta', 'jk_add_item_sku', 10, 3 );
function jk_add_item_sku( $item_id, $values, $cart_item_key ) {

	$item_sku = get_post_meta( $values[ 'product_id' ], '_sku', true );

	wc_add_order_item_meta( $item_id, 'sku', $item_sku , false );

}

/* Flush rewrite rules for custom post types. */
// flush_rewrite_rules(true);
// global $wp_rewrite; $wp_rewrite->flush_rules();