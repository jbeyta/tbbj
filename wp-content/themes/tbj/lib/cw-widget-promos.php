<?php
class cw_promos extends WP_Widget {

function __construct() {
	parent::__construct(
		// Base ID of your widget
		'cw_promos', 

		// Widget name will appear in UI
		__('CW Promos', 'cw_promos_domain'),

		// Widget description
		array( 'description' => __( 'Display a Promo Image', 'cw_promos_domain' ), ) 
	);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
	$title = apply_filters( 'widget_title', $instance['title'] );
	$position = $instance['position'];

	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	if ( ! empty( $title ) ) {
		echo $args['before_title'] . $title . $args['after_title'];
	}

	$class = '';
	if($make_button == 'on') {
		$class = 'button';
	}

	// This is where you run the code and display the output
	cw_get_promo($position);

	echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	} else {
		$title = __( '', 'cw_promos_domain' );
	}

	if ( isset( $instance[ 'position' ] ) ) {
		$position = $instance[ 'position' ];
	} else {
		$position = '';
	}

	// Widget admin form
?>

<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
<br><br>
<select id="<?php echo $this->get_field_id( 'position' ); ?>" name="<?php echo $this->get_field_name( 'position' ); ?>" >
<option value="">Select a Position</option>
<?php
	$terms = get_terms('promos_categories', array('hide_empty' => false));
	if(!empty($terms)) {
		foreach ($terms as $term) {
			$selected = '';
			if($position == $term->slug) {
				$selected = 'selected';
			}
			echo'<option value="'.$term->slug.'" '.$selected.'>'.$term->name.'</option>';
		}
	}
?>
</select>
</p>

<?php }

			// Updating widget replacing old instances with new
			public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['position'] = ( ! empty( $new_instance['position'] ) ) ? strip_tags( $new_instance['position'] ) : '';
			$instance['ext_url'] = ( ! empty( $new_instance['ext_url'] ) ) ? strip_tags( $new_instance['ext_url'] ) : '';
			$instance['make_button'] = ( ! empty( $new_instance['make_button'] ) ) ? strip_tags( $new_instance['make_button'] ) : '';
			return $instance;
		}
	} // Class cw_sidenav ends here

// Register and load the widget
function cw_promos_load_widget() {
	register_widget( 'cw_promos' );
}
add_action( 'widgets_init', 'cw_promos_load_widget' );