<?php
// ------------------------------------
//
// Custom Meta Boxes
//
// ------------------------------------
// custom meta boxes
function cw_list_services() {
	if(!is_admin()) {
		return;
	}

	$sargs = array(
		'post_type' => 'services',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	global $cw_services_list;
	global $cw_services_ids;
	$cw_services_list = array();
	$cw_services_ids = array();

	$servs = new WP_Query($sargs);
	if($servs->have_posts()) {
		while($servs->have_posts()) {
			$servs->the_post();
			global $post;
			$cw_services_list[$post->ID] = get_the_title();
			array_push($cw_services_ids, $post->ID);
		}
	}
}
add_action('init', 'cw_list_services');

function list_pages() {
	if(!is_admin()) {
		return;
	}
	global $post;

	$pages_args = array(
		'post_type' => 'page',
		'order' => 'ASC',
		'orderby' => 'title',
		'posts_per_page' => -1
	);

	$pages = new WP_Query($pages_args);

	global $cw_page_list;
	$cw_page_list = array('' => 'Select a page');

	if($pages->have_posts()) {
		while($pages->have_posts()) {
			$pages->the_post();
			
			$cw_page_list[$post->ID] = get_the_title();
		}
	}
	wp_reset_query();
}
add_action('init', 'list_pages');

function cw_list_media() {
	if(!is_admin()) {
		return;
	}
	
	$margs = array(
		'post_type' => 'attachment',
		'post_mime_type' =>'application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'post_status' => 'inherit',
		'posts_per_page' => -1
	);

	$media = new WP_Query( $margs );

	global $cw_media_list;
	$cw_media_list = array('' => 'Select a file');
	foreach($media->posts as $file) {
		$cw_media_list[$file->ID] = $file->post_title;
	}
}
add_action('init', 'cw_list_media');

function cw_metaboxes( array $meta_boxes ) {
	global $cw_page_list;
	global $cw_services_list;
	global $cw_services_ids;
	// use for select, checkbox, radio of list of states
	global $cw_states;

	$prefix = '_cwmb_'; // Prefix for all fields

	$slides = new_cmb2_box( array(
		'id'            => $prefix.'slides',
		'title'         => 'Slide Info',
		'object_types'  => array( 'slides', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );


	$slides->add_field( array(
		'name' => 'Image',
		'desc' => 'Images must be at least 1400px x 600px',
		'id' => $prefix.'slide_image',
		'type' => 'file'
	) );

	$slides->add_field( array(
		'name' => 'Caption',
		'desc' => 'Max Characters: 150',
		'id' => $prefix.'slide_caption',
		'type' => 'textarea',
		// uncomment below to unlock character limit for this field
		'attributes'  => array(
			'maxlength' => 150,
			'onkeyup' => "textCounter(this, 'cmb2-metabox-description', 150);"
		),
		'after_row' => '
		<script>
			function textCounter(field, field2, maxlimit) {
				if(!jQuery("."+field2).find(".remain").length) {
					jQuery("."+field2).append("<br><span class=\'remain\'></span>");
				}

				jQuery(".remain").html(maxlimit - field.value.length+" out of "+maxlimit);
			}
		</script>'
	) );

	$slides->add_field( array(
		'name' => 'Link',
		'id' => $prefix.'slide_link',
		'type' => 'text_url'
	) );


	// locations
	$location = new_cmb2_box( array(
		'id'            => $prefix.'locations',
		'title' => 'Location Info',
		'object_types'  => array( 'locations', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$location->add_field( array(
		'name' => 'Address 1',
		'id' => $prefix.'loc_address1',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Address 2',
		'id' => $prefix.'loc_address2',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'City',
		'id' => $prefix.'loc_city',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'State',
		'id' => $prefix.'loc_state',
		'type' => 'select',
		'options' => $cw_states
	) );

	$location->add_field( array(
		'name' => 'Zip',
		'id' => $prefix.'loc_zip',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Phone',
		'id' => $prefix.'loc_phone',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Phone 2',
		'id' => $prefix.'loc_phone2',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Fax',
		'id' => $prefix.'loc_fax',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Email',
		'id' => $prefix.'loc_email',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Photo',
		'id' => $prefix.'loc_photo',
		'type' => 'file'
	) );

	$location->add_field( array(
		'name' => 'Hours',
		'id' => $prefix.'loc_hours',
		'type' => 'textarea'
	) );

	$location->add_field( array(
		'name' => 'Lat',
		'desc' => 'In the case that Google maps show the inccorrect location, use lat & lon',
		'id' => $prefix.'loc_lat',
		'type' => 'text'
	) );

	$location->add_field( array(
		'name' => 'Lon',
		'desc' => 'In the case that Google maps show the inccorrect location, use lat & lon',
		'id' => $prefix.'loc_lon',
		'type' => 'text'
	) );

	// promos
	$promos = new_cmb2_box( array(
		'id'            => $prefix.'promo',
		'title'         => 'Promo Info',
		'object_types'  => array( 'promos', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$promos->add_field( array(
		'name' => 'Promo Image',
		'id' => $prefix.'promo_image',
		'type' => 'file'
	) );

	$promos->add_field( array(
		'name' => 'Link',
		'id' => $prefix.'promo_link',
		'type' => 'text_url'
	) );

	// services
	$services = new_cmb2_box( array(
		'id'            => $prefix.'service_excerpt',
		'title' => 'Options',
		'object_types'  => array( 'services', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$services->add_field( array(
		'name' => 'Service Image',
		'id' => $prefix.'service_image',
		'type' => 'file'
	) );

	// testimonials
	$testimonials = new_cmb2_box( array(
		'id'            => $prefix.'testimonials',
		'title' => 'Testimonial',
		'object_types'  => array( 'testimonials', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$testimonials->add_field( array(
		'id' => $prefix.'testimonial',
		'type' => 'textarea'
	) );
	$testimonials->add_field( array(
		'name' => 'Vocation',
		'id' => $prefix.'vocation',
		'type' => 'text'
	) );
	$testimonials->add_field( array(
		'name' => 'Location',
		'id' => $prefix.'location',
		'type' => 'text'
	) );

	// staff
	$staff = new_cmb2_box( array(
		'id'            => $prefix.'staff',
		'title' => 'Testimonial',
		'object_types'  => array( 'staff', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$staff->add_field( array(
		'name' => 'Title',
		'id' => $prefix.'staff_title',
		'type' => 'text'
	) );
	$staff->add_field( array(
		'name' => 'Image',
		'id' => $prefix.'staff_image',
		'type' => 'file'
	) );
	$staff->add_field( array(
		'name' => 'Bio',
		'id' => $prefix.'staff_bio',
		'type' => 'textarea'
	) );

	// galleries
	$galleries = new_cmb2_box( array(
		'id'            => $prefix.'galleries',
		'title' => 'Gallery Info',
		'object_types'  => array( 'galleries', ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names'    => true, // Show field names on the left
	) );

	$galleries->add_field( array(
		'name' => 'Images',
		'id' => $prefix.'gallery_images',
		'type' => 'file_list'
	) );

	$galleries->add_field( array(
		'name' => 'Description',
		'id' => $prefix.'gallery_desc',
		'type' => 'textarea'
	) );

	// schedule
	$schedule_loc_meta = new_cmb2_box( array(
		'id'            => $prefix.'schedule',
		'title' => 'Info',
		'object_types'  => array( 'classes', ), // Post type
		'context' => 'normal',
		'priority' => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$schedule_loc_meta->add_field( array(
		'name' => 'Video URL',
		'desc' => 'Video embeds will not work in editor above. Please use this field instead.<br><b>NOTE: Use the url to the video, not the embed code.</b><br><span style="color:green">Correct: https://www.youtube.com/watch?v=dQw4w9WgXcQ</span><br><span style="color:red">Wrong: &lt;iframe width="560" height="315" src="https://www.youtube.com/embe...</span>',
		'id' => $prefix.'video',
		'type' => 'oembed'
	) );

	$schedule_loc_meta->add_field( array(
		'name' => 'Room',
		'id' => $prefix.'room',
		'type' => 'text'
	) );

	$schedule_meta_group = new_cmb2_box( array(
		'id' => $prefix . 'schedule_meta',
		'title' => esc_html__( 'Info', 'cmb2' ),
		'object_types' => array( 'classes', ),
	) );

	$schedule_meta = $schedule_meta_group->add_field( array(
		'id' => $prefix . 'schedule_infos',
		'type' => 'group',
		'options' => array(
			'group_title'   => esc_html__( 'Day {#}', 'cmb2' ),
			'add_button'    => esc_html__( 'Add Another Day', 'cmb2' ),
			'remove_button' => esc_html__( 'Remove Day', 'cmb2' ),
			'sortable'      => true,
		),
	) );

	$schedule_meta_group->add_group_field( $schedule_meta, array(
		'name' => esc_html__( 'Days of the Week', 'cmb2' ),
		'id' => 'day',
		'type' => 'radio',
		'options' => array(
			'monday' => 'Monday',
			'tuesday' => 'Tuesday',
			'wednesday' => 'Wednesday',
			'thursday' => 'Thursday',
			'friday' => 'Friday',
			'saturday' => 'Saturday',
			'sunday' => 'Sunday',
		)
	) );

	$schedule_meta_group->add_group_field( $schedule_meta, array(
		'name' => esc_html__( 'Times', 'cmb2' ),
		'desc' => 'One time per line. Example:<br>10:00 am<br>1:30 pm<br>5:00 pm<br><b>NOTE: Avoid typos, extra spaces, etc. as it can mess up the sorting.</b>',
		'id' => 'times',
		'type' => 'textarea'
	) );

	$schedule_meta_group->add_group_field( $schedule_meta, array(
		'name' => esc_html__( 'Teacher', 'cmb2' ),
		'id' => 'teacher',
		'type' => 'text'
	) );

	// alerts
	$alerts = new_cmb2_box( array(
		'id' => $prefix.'alerts',
		'title' => 'Alert Info',
		'object_types' => array( 'alerts' ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
	) );

	$alerts->add_field( array(
		'name' => 'Alert Text',
		'id' => $prefix.'alert_text',
		'type' => 'textarea'
	) );

	$alerts->add_field( array(
		'name' => 'Alert Expiration Date',
		'id' => $prefix.'alert_expire',
		'type' => 'text_datetime_timestamp'
	) );

	// // product_previews
	// $product_previews_metaboxes = new_cmb2_box( array(
	// 	'id'           => $prefix . 'product_previews_metabox',
	// 	'title'        => esc_html__( 'PDF Previews', 'cmb2' ),
	// 	'object_types' => array( 'product', ),
	// ) );
	// // $group_field_id is the field id string, so in this case: $prefix . 'demo'
	// $product_previews = $product_previews_metaboxes->add_field( array(
	// 	'id'          => $prefix . 'previews',
	// 	'type'        => 'group',
	// 	'description' => esc_html__( 'Upload Preiew', 'cmb2' ),
	// 	'options'     => array(
	// 		'group_title'   => esc_html__( 'Preview {#}', 'cmb2' ), // {#} gets replaced by row number
	// 		'add_button'    => esc_html__( 'Add Another Preview', 'cmb2' ),
	// 		'remove_button' => esc_html__( 'Remove Preview', 'cmb2' ),
	// 		'sortable'      => true, // beta
	// 	),
	// ) );

	// $product_previews_metaboxes->add_group_field( $product_previews, array(
	// 	'name'       => 'Title',
	// 	'id'         => 'title',
	// 	'type'       => 'text',
	// ) );

	// $product_previews_metaboxes->add_group_field( $product_previews, array(
	// 	'name'       => 'File',
	// 	'desc' => 'PDF only',
	// 	'id'         => 'file',
	// 	'type'       => 'file',
	// 	'query_args' => array(
	// 		'type' => 'application/pdf', // Make library only display PDFs.
	// 	),
	// ) );

	// documents
	$documents = new_cmb2_box( array(
		'id' => $prefix.'documents',
		'title' => 'Info',
		'object_types' => array( 'documents' ), // Post type
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => True, // Show field names on the left
	) );

	$documents->add_field( array(
		'name' => 'Subtitle',
		'id' => $prefix.'subtitle',
		'type' => 'text'
	) );

	$documents->add_field( array(
		'name' => 'File',
		'id' => $prefix.'file',
		'type' => 'file',
		'query_args' => array(
			'type' => 'application/pdf', // Make library only display PDFs.
		),
	) );


	return $meta_boxes;
}
add_filter( 'cmb2_meta_boxes', 'cw_metaboxes' );

// end custom meta boxes

function cmb_show_on_meta_value( $display, $meta_box ) {
	if ( ! isset( $meta_box['show_on']['key'] ) || $meta_box['show_on']['key'] != 'preview' ) {
		return $display;
	}

	$allow = cw_options_get_option('_cwo_allow_dev_notes');

	if(empty($allow)) {
		return false;
	}

	return true;
}
add_filter( 'cmb2_show_on', 'cmb_show_on_meta_value', 10, 2 );