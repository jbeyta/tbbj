<?php
class cw_featured_prods extends WP_Widget {

function __construct() {
	parent::__construct(
		// Base ID of your widget
		'cw_featured_prods', 

		// Widget name will appear in UI
		__('Featured Products', 'cw_featured_prods_domain'),

		// Widget description
		array( 'description' => __( 'Displays Featured (Starred) Products', 'cw_featured_prods_domain' ), ) 
	);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
	$title = apply_filters( 'widget_title', $instance['title'] );
	$number = $instance['number'];

	// before and after widget arguments are defined by themes
	echo $args['before_widget'];
	if ( ! empty( $title ) ) {
		echo $args['before_title'] . $title . $args['after_title'];
	}

	// This is where you run the code and display the output
	
	global $post;

	$pargs = array(
		'post_type' => 'product',
		'posts_per_page' => $number,
		'orderby' => 'rand',
		// 'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'product_visibility',
				'field' => 'slug',
				'terms' => 'featured'
			)
		)
	);

	$prods = new WP_Query($pargs);
	if($prods->have_posts()) {
		echo '<div class="flex-row grid">';
		while($prods->have_posts()) {
			$prods->the_post();
			// echo_pre(get_post_meta($post->ID));

			$size = (string) 12 / $number;

			if($number >= 5) {
				$size = 3;
			}

			echo '<div class="featured-product s12 m'.$size.'">';
				$img = get_the_post_thumbnail_url($post->ID, 'full');
				$cropped = aq_resize($img, 640, 640, true, true, true);
				if(!empty($cropped)) {
					echo '<a href="'.get_the_permalink().'"><img src="'.$cropped.'" alt="'.get_the_title().'" /></a>';
				}
				echo '<h5><a href="'.get_the_permalink().'">'.get_the_title().'</a></h5>';
			echo '</div>';
			// echo_pre(get_post_meta($post->ID));
		}
		echo '</div>';
	}
	wp_reset_query();


	echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
	if ( isset( $instance[ 'title' ] ) ) {
		$title = $instance[ 'title' ];
	} else {
		$title = __( '', 'cw_featured_prods_domain' );
	}

	if ( isset( $instance[ 'number' ] ) ) {
		$number = $instance[ 'number' ];
	} else {
		$number = 2;
	}

	// Widget admin form
?>

<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
<br><br>
<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of Products (default is 2):' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
</p>

<?php }

			// Updating widget replacing old instances with new
			public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['number'] = ( ! empty( $new_instance['number'] ) ) ? strip_tags( $new_instance['number'] ) : '';
			return $instance;
		}
	} // Class cw_sidenav ends here

// Register and load the widget
function cw_featured_prods_load_widget() {
	register_widget( 'cw_featured_prods' );
}
add_action( 'widgets_init', 'cw_featured_prods_load_widget' );